alias config='/usr/bin/git --git-dir=$HOME/.mycfg/ --work-tree=$HOME'
alias cask='brew cask'
alias reset='cd ~;source ~/.zshrc;clear'
alias dmod=dark-mode
alias vim=nvim
alias vshort='vim ~/workshop/notes/nvim'
alias tmun='tmux new-session -t'
alias mdb='mongod -f /usr/local/etc/mongod.conf'
alias glog='git log --all --decorate --graph'
alias glogo='git log --all --decorate --all --oneline'
alias cglog='cg log --all --decorate --graph'
alias cglogo='cg log --all --decorate --all --oneline'
alias grep='grep --color=auto'
alias ls=exa
alias ll='exa -l'
alias l='/bin/ls'
